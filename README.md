Introduzione:
- Progetto di un Autosalone contenente i costrutti necessari per rappresentare le Auto,i proprietari(Aziende oppure Persone) e gli Autosaloni nei quali le auto verranno esposte.

Requirements:

- Ogni Concessionario avrà il proprio Autosalone contenente i veicoli in esposizione e la lista dei veicoli venduti.
- Ogni Veicolo potrà essere venduto in un solo Autosalone per volta.
- Ogni Veicolo dovrà avere obbligatoriamente un proprietario che potrà essere un privato (quindi una Persona), un'Azienda o eventualmente il Concessionario.
- Ogni Persona dovrà essere composta da informazioni tali da poterla identificare univocamente 



User Manual:


● Persona:                                                                                                                                                                                              
- Classe contenente le informazioni basilari per rappresentare una Persona, composta da Nome,Cognome,Data,Residenza                                                                                      
- La data viene rappresentata secondo il formato "dd-MM-yyyy" e presenta un controllo sull'inserimento corretto tramite l'exception "InvalidDateException"


● InvalidDateException:                                                                                                                                                                                 
- Exception utilizzata per evitare l'inserimento di una data inconsistente.

● Azienda:                                                                                                                                                                                              
- Classe contenente il nome dell'azienda

● Auto:                                                                                                                                                                                                 
- Classe contenente tutte le informazioni utili a rappresentare un veicolo generico, composta da Marca,Nome,Cilindrata,capacitaSerbatoio,KmperLitro,StatoMacchina e  proprietario della macchina(Persona o Azienda)

● Concessionario:                                                                                                                                                                                       
- Classe contenente le informazioni per rappresentare un concessionario, composta  dall'ArrayList utile a salvare le Auto e dai metodi:                                                                  
    -----> cambiaCilindrata: Usato per modificare la cilindrata di un'auto presente in un Autosalone                                                                                                            
    -----> calcolaAutonomiaAuto: Usato per calcolare l'autonomia di un'auto moltiplicando I km per litro con la capacità del serbatoio                                                                          
    -----> StatoMacchina: Usato per controllare se la macchina è stata già venduta ad un'azienda/in esposizione/ venduta ad un privato                                                                                  
    -----> getDati: Usato per acquisire il nome dell'Auto, il proprietario e lo stato della macchina. Sono quindi presenti tre metodi aventi lo stesso nome per i tre casi possibili                            
    -----> DetentoreAuto: Usato per risalire al proprietario dell'Auto. Se quest'ultima non è stata ancora venduta il risultato sarà nullo                                                                      

● ProjectRunner:                                                                                                                                                                                        
- Classe nella quale è presente il main e le varie inizializzazioni delle Auto,Concessionari,Persone,Aziende con degli output di prova per testare i vari metodi e le varie classi presenti nel progetto                     
                                                                                                                                                                                                                    
