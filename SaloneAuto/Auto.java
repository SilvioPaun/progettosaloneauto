import java.lang.String;

public class Auto {
       
    private String Marca; 
    private String Nome;
    private int Cilindrata;
    private int capacitaSerbatoio;
    private int KmperLitro;
    private String StatoMacchina;
    private Persona persona;
    private Azienda azienda;
    
	
    public Auto(String marca, String nome, int cilindrata, int capacitaSerbatoio, int kmperLitro,String statomacchina) {
	    Marca = marca;
		Nome = nome;
		Cilindrata = cilindrata;
		this.capacitaSerbatoio = capacitaSerbatoio;
		KmperLitro = kmperLitro;
		StatoMacchina = statomacchina;
		persona = null;
		azienda = null;
	}

	@Override
	public String toString() {
		return "[Marca=" + Marca + ", Nome=" + Nome + ", Cilindrata=" + Cilindrata + ", capacitaSerbatoio="
				+ capacitaSerbatoio + ", KmperLitro=" + KmperLitro + ", StatoMacchina=" + StatoMacchina + ", persona="
				+ persona + ", azienda=" + azienda + "]"+"\n";
	}

	public String getMarca() {
		return Marca;
	}

	public void setMarca(String marca) {
		Marca = marca;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public int getCilindrata() {
		return Cilindrata;
	}

	public void setCilindrata(int cilindrata) {
		Cilindrata = cilindrata;
	}

	public int getCapacitaSerbatoio() {
		return capacitaSerbatoio;
	}

	public void setCapacitaSerbatoio(int capacitaSerbatoio) {
		this.capacitaSerbatoio = capacitaSerbatoio;
	}

	public int getKmperLitro() {
		return KmperLitro;
	}

	public void setKmperLitro(int kmperLitro) {
		KmperLitro = kmperLitro;
	}

	public String getStatoMacchina() {
		return StatoMacchina;
	}

	public void setStatoMacchina(String statoMacchina) {
		StatoMacchina = statoMacchina;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Azienda getAzienda() {
		return azienda;
	}

	public void setAzienda(Azienda azienda) {
		this.azienda = azienda;
	}


}
