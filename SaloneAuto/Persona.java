import java.lang.String;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Persona {
	private String Nome;
	private String Cognome;
	private String Data;
	private String Residenza;
	SimpleDateFormat dateFormat;
	Date dataDiNascita;
	DateFormat df = new SimpleDateFormat("dd-MM-yyyy");

	public Persona(String nome, String cognome, String data, String residenza) throws InvalidDateException {

		int x;

		String[] dataArray = data.split("-", 5);

		x = Integer.parseInt(dataArray[0]);
		if (x > 31 || x < 1) {
			throw new InvalidDateException("Giorno inserito in modo errato");
		}
		x = Integer.parseInt(dataArray[1]);
		if (x > 12 || x < 1) {
			throw new InvalidDateException("Mese inserito in modo errato");
		}
		x = Integer.parseInt(dataArray[2]);
		if (x < 1900) {
			throw new InvalidDateException("Anno inserito in modo errato");
		}

		Nome = nome;
		Cognome = cognome;
		Data = data;
		Residenza = residenza;
	}

	public String getNome() {
		return Nome;
	}

	public String getCognome() {
		return Cognome;
	}

	public int getGiornoNascita() throws java.text.ParseException {

		int giorno;
		dataDiNascita = df.parse(Data);
		giorno = dataDiNascita.getDate();

		return giorno;

	}

	public int getMeseNascita() throws java.text.ParseException {

		int mese;
		dataDiNascita = df.parse(Data);
		mese = dataDiNascita.getMonth() + 1;

		return mese;

	}

	public int getAnnoNascita() throws java.text.ParseException {
		int anno;
		dataDiNascita = df.parse(Data);
		anno = dataDiNascita.getYear();
		anno = anno + 1900;
		return anno;

	}

	public String getResidenza() {
		return Residenza;
	}

	public String getData() {
		return Data;
	}

	@Override
	public String toString() {
		return Nome + " " + Cognome + " " + Data;
	}

}
