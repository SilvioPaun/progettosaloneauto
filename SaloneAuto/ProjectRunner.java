import java.util.ArrayList;

public class ProjectRunner {


	public static void main(String[] args) throws InvalidDateException {
		
		//Creazione degli arraylist di tipo Auto contenenti le macchine per ogni salone
		ArrayList<Auto> salone = new ArrayList<Auto>();
		ArrayList<Auto> salone2 = new ArrayList<Auto>();
		ArrayList<Auto> salone3 = new ArrayList<Auto>();
		
		//Creazione dei vari concessionari
		Concessonario autoRoma= new Concessonario(salone);
		Concessonario autoFiori= new Concessonario(salone2);
		Concessonario autoLava= new Concessonario(salone3);
		
		//Inizializzazione delle Persone
		Persona p= null;
		Persona p2= null;
		Persona p3= null;
		
		//Try & catch utilizzata per evitare errori di inserimento della data
		try {
			p=new Persona("Marco", "Bruni","12-01-1990", "Roma");
			p2=new Persona("Luca", "Seri","21-11-1996", "Milano");
			p3=new Persona("Alex", "Rossi","22-02-1979", "Torino");
			
		}catch(InvalidDateException e) {
			
			System.err.println("La data e' stata inserita in modo errato! \n");
			e.printStackTrace(); 
		}
		
		//Creazione delle Aziende 
		Azienda Ritoli= new Azienda("Ritoli");
		
		//Creazione delle Auto
		Auto a1 = new Auto("Audi", "A1", 2500, 50, 10, "VendutaPrivato");
		Auto a2= new Auto("Audi", "A2", 3000, 80, 5, "VendutaPrivato");
		Auto a3 = new Auto("Volkswagen", "A3", 2000, 40, 8, "VendutaAzienda");
		Auto a4 = new Auto("Fiat", "A4", 1000, 23, 12, "VendutaAzienda");
		Auto a5= new Auto("Dacia", "A5", 2500, 76, 2, "VendutaPrivato");
		Auto a6 = new Auto("Volkswagen", "A6", 345, 88, 67, "VendutaPrivato");
		Auto a7 = new Auto("Fiat", "A7", 5552, 28, 89, "InEsposizione");
		Auto a8 = new Auto("Dacia", "A8", 1212, 33, 64, "InEsposizione");
		
		//Set delle aziende che hanno acquistato delle macchine
		a3.setAzienda(Ritoli);
		a4.setAzienda(Ritoli);
		
		//Inserimento delle auto nel salone specifico
		salone.add(a1);
		salone.add(a2);
		salone.add(a3);
		salone2.add(a4);
		salone2.add(a5);
		salone3.add(a6);
		salone.add(a7);
		salone.add(a8);
		
		
		//Associazione macchina<--->proprietario/stato macchina
		autoRoma.getDati("A1", p, a1.getStatoMacchina());
		autoRoma.getDati("A2", p, a2.getStatoMacchina());
		autoRoma.getDati("A3", Ritoli, a3.getStatoMacchina());
		autoFiori.getDati("A4", Ritoli, a4.getStatoMacchina());
		autoFiori.getDati("A5", p2, a5.getStatoMacchina());
		autoLava.getDati("A6", p3, a6.getStatoMacchina());
		autoRoma.getDati("A7",a7.getStatoMacchina());
		autoRoma.getDati("A8",a8.getStatoMacchina());
		
		
		//Output generici
		
//		System.out.println(a8.toString());
//		System.out.println(salone.toString());
//		System.out.println(autoFiori.DetentoreAuto(a2));
//		System.out.println(autoFiori.StatoMacchina("A4"));
//		autoFiori.cambiaCilindrata(a5, 3333);
//		System.out.println(a5.toString());

	}

}
