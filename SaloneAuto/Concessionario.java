import java.util.ArrayList;

public class Concessionario {

	private ArrayList<Auto> Salone;

	public Concessionario(ArrayList<Auto> salone) {
		Salone = salone;
	}

	public void cambiaCilindrata(Auto auto, int cilindrata) {
		auto.setCilindrata(cilindrata);

	}

	public int calcolaAutonomiaAuto(Auto auto) { 
		int x = auto.getCapacitaSerbatoio();
		int y = auto.getKmperLitro();
		return y * x;
	}

	public String StatoMacchina(String nomeAuto) { 
		String statoM = "";
		for (Auto a : Salone) {
			if (a.getNome().equalsIgnoreCase(nomeAuto)) {
				statoM = a.getStatoMacchina();
			}
		}
		return statoM;

	}
	
	public void getDati(String nomeAuto, Azienda azienda, String statoMacchina) {
																					
		for (Auto x : Salone) {
			if (x.getNome().equalsIgnoreCase(nomeAuto)) {
				x.setAzienda(azienda);
				x.setStatoMacchina(statoMacchina);

			}
		}
	}

	public void getDati(String nomeAuto, Persona persona, String statoMacchina) { 
																												
		for (Auto x : Salone) {
			if (x.getNome().equalsIgnoreCase(nomeAuto)) {
				x.setPersona(persona);
				x.setStatoMacchina(statoMacchina);
			}
		}
	}
	
	public void getDati(String nomeAuto ,String statoMacchina) { 
		
		for (Auto x : Salone) {
			if (x.getNome().equalsIgnoreCase(nomeAuto)) {
				x.setStatoMacchina(statoMacchina);
			}
		}
	}
	
	
	public String DetentoreAuto(Auto auto) {
		String detenzione = "";

		if (auto.getStatoMacchina() == "VendutaPrivato") {

			detenzione = auto.getPersona().getNome() + " " + auto.getPersona().getCognome();

		} else if (auto.getStatoMacchina() == "VendutaAzienda") {

			if (auto.getAzienda() != null) {

				detenzione = auto.getAzienda().getNome();
			}

		}else {
			
			System.out.println("L'Auto non è stata ancora venduta \n");
			
		}

		return detenzione;

	}

}
